Android interface
-----------------

When VLC is fully installed and opened, it scans through your device to 
get videos and audios.

The image below shows the main inferface after loading.

.. note:: the circled numbers are labels and are not part of the VLC for android application interface.


.. image:: /images/gettingstarted/interface/android/vlc-android-video-interface.png
    :align: center
    :width: 40%


1. Search bar.

2. Sort by.

3. Sub menu.

4. Video tab.

5. Audio tab.

6. Browser tab.

7. Playlist tab.

8. More options (settings, about VLC, and History)

9. Play all Button.

10. Video tab view.


########################
Video playback inferface
########################

When you tap on the any video it takes you to the video playback interface
as shown below.

.. image:: /images/gettingstarted/interface/android/vlc-video-playback-interface.png
        :align: center
        :width: 40%



#########
Audio tab
#########

When you click on the audio tab you are taken to the 
*audio-tab-view* as shown below.


.. image:: /images/gettingstarted/interface/android/vlc-android-audio-interface.png
    :align: center
    :width: 40%


1. **Artist tab:** shows the *artist-tab-view* with all avaliable artists.

2. **Albums tab:** shows the *ablum-tab-view* with all avaliable albums.

3. **Tracks tab:** shows the *tracks-tab-view* with all avaliable tracks.

4. **Genres tab:** shows the *genres-tab-view* with all avaliable genres.

5. Artist tab view.


When you click on any song VLC takes you to the audio playback inferface
as shown below.

.. image:: /images/gettingstarted/interface/android/vlc-android-audio-playback.png
        :align: center
        :width: 40%

1. Swipe left or right to change song.

2. Swipe up or down to reveal or hide playlist.

3. Hold down the pause button to stop the song.


On the playlist view is as shown below.

.. image:: /images/gettingstarted/interface/android/vlc-android-audio-playlist-interface.png
        :align: center
        :width: 40%

1. Swipe left or right to remove song from playlist.

2. Hold down a song and drag it up or down to rearrange the playlist.

3. Seek backward or forward.

4. Shuffle songs.

5. Repeat song.


###########
Browser tab
###########

.. image:: /images/gettingstarted/interface/android/vlc-android-browse-interface.png
    :align: center
    :width: 40%

The browser tab is for:

1. Browsing through folder on your phone.

2. Adding local networks.

3. Viewing your local networks.



############
Playlist tab
############

This tab is for viewing saved playlist.

.. image:: /images/gettingstarted/interface/android/vlc-android-playlist-interface.png
    :align: center
    :width: 40%


################
More options tab
################

.. image:: /images/gettingstarted/interface/android/vlc-android-browse-interface.png
    :align: center
    :width: 40%

The more options tab is for:

1. Changing settings on your VLC.

2. Viewing details about VLC, version installed and licence.

3. Streaming videos or audios.

4. History of recently played videos and audios.

.. toctree::
   :maxdepth: 2
   :hidden:
   :name: toc-interface
